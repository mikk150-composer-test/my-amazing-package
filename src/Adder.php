<?php

namespace Mikk\MyAmazingPackage;

class Adder
{
    public function add()
    {
        return array_reduce(func_get_args(), function ($carry, $item) {
            return $carry + $item;
        }, 0);
    }
}
